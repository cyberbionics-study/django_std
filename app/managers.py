import sqlite3

from app.models import Actor


DB_PATH = "C:/Users/38098/Desktop/учеба/CyberBionics/homework/моё/django/django_std/app/actors.sqlite"


class ActorManager:
    def __init__(self):
        self.conn = sqlite3.connect(DB_PATH)
        self.table_name = "actors"

    def all(self):
        sql = f"SELECT * FROM {self.table_name};"
        try:
            actors_cursor = self.conn.execute(sql)
            return [Actor(*row) for row in actors_cursor]
        except sqlite3.Error as error:
            print(f"Error: {error}")

    def create(self, first_name: str, last_name: str):
        sql = f"INSERT INTO {self.table_name}(first_name, last_name) VALUES (?, ?);"
        params = (first_name, last_name)
        try:
            self.conn.execute(sql, params)
        except sqlite3.Error as error:
            print(f"Error: {error}")
