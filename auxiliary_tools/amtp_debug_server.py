import asyncio
from aiosmtpd.controller import Controller
from aiosmtpd.handlers import Debugging


async def main():
    controller = Controller(Debugging(), hostname="localhost", port=1025)
    controller.start()

    try:
        await asyncio.sleep(3600)
    except KeyboardInterrupt:
        controller.stop()


if __name__ == "__main__":
    asyncio.run(main())
