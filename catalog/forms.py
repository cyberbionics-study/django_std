from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.forms import CheckboxSelectMultiple, CheckboxInput

from catalog.models import LiteraryFormat, Book, Author


class LiteraryFormatForm(forms.ModelForm):
    # MIN_NAME_LENGTH = 3
    # name = forms.CharField(
    #     required=True, validators=[MinLengthValidator(MIN_NAME_LENGTH)], max_length=63
    # )

    class Meta:
        model = LiteraryFormat
        fields = (
            "name",
            "description",
        )

    # def clean_name(self):
    #     name = self.clean_name["name"]
    #     if len(name) < self.MIN_NAME_LENGTH:
    #         raise ValidationError(
    #             f"Name of literary format must be at least {self.MIN_NAME_LENGTH} characters."
    #         )
    #     return name


class BookForm(forms.ModelForm):
    authors = forms.ModelMultipleChoiceField(
        queryset=Author.objects.all(), widget=CheckboxSelectMultiple, required=False
    )

    format = forms.ModelChoiceField(
        queryset=LiteraryFormat.objects.all(),
        widget=CheckboxSelectMultiple,
        required=True,
    )

    class Meta:
        model = Book
        fields = "__all__"


class BookSearchForm(forms.Form):
    title = forms.CharField(
        max_length=255,
        required=False,
        label="",
        widget=forms.TextInput(attrs={"placeholder": "Search by title"}),
    )


class AuthorSearchForm(forms.Form):
    last_name = forms.CharField(max_length=63, required=False)
