from django.urls import path
from .views import (
    index,
    LiteraryFormatListView,
    BookListView,
    AuthorListView,
    BookDetailView,
    LiteraryFormatCreateView,
    LiteraryFormatUpdateView,
    LiteraryFormatDeleteView,
    AuthorDetailView,
    BookCreateView,
    BookUpdateView,
    BookDeleteView,
    AuthorUpdateView,
    AuthorDeleteView,
)

app_name = "catalog"

urlpatterns = [
    path("", index, name="index"),
    path(
        "literary-formats/",
        LiteraryFormatListView.as_view(),
        name="literary_format_list",
    ),
    path(
        "literary-formats/create",
        LiteraryFormatCreateView.as_view(),
        name="literary_format_create",
    ),
    path(
        "literary-formats/<int:pk>update/",
        LiteraryFormatUpdateView.as_view(),
        name="literary_format_update",
    ),
    path(
        "literary-formats/<int:pk>delete/",
        LiteraryFormatDeleteView.as_view(),
        name="literary_format_delete",
    ),
    path("books/", BookListView.as_view(), name="book_list"),
    path("books/<int:pk>/", BookDetailView.as_view(), name="book_detail"),
    path(
        "books/create",
        BookCreateView.as_view(),
        name="book_create",
    ),
    path(
        "books/<int:pk>update/",
        BookUpdateView.as_view(),
        name="book_update",
    ),
    path(
        "books/<int:pk>delete/",
        BookDeleteView.as_view(),
        name="book_delete",
    ),
    path("authors/", AuthorListView.as_view(), name="author_list"),
    path("authors/<int:pk>/", AuthorDetailView.as_view(), name="author_detail"),
    path(
        "authors/create",
        BookCreateView.as_view(),
        name="author_create",
    ),
    path(
        "authors/<int:pk>update/",
        AuthorUpdateView.as_view(),
        name="author_update",
    ),
    path(
        "authors/<int:pk>delete/",
        AuthorDeleteView.as_view(),
        name="author_delete",
    ),
]
