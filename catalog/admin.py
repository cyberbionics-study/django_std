from django.contrib import admin
from .models import LiteraryFormat, Book, Author


admin.site.register(LiteraryFormat)
admin.site.register(Book)
admin.site.register(Author)
