from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import (
    HttpRequest,
    HttpResponse,
    Http404,
    HttpResponseRedirect,
    JsonResponse,
)
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views import generic

from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework import status, generics, mixins, viewsets
from rest_framework.views import APIView

from catalog.forms import LiteraryFormatForm, BookForm, BookSearchForm, AuthorSearchForm
from catalog.models import Book, Author, LiteraryFormat
from catalog.serializators import (
    LiteraryFormatSerializer,
    AuthorSerializer,
    BookSerializer,
)


def index(request: HttpRequest) -> HttpResponse:
    num_books = Book.objects.all().count()
    num_authors = Author.objects.all().count()
    num_literary_formats = LiteraryFormat.objects.all().count()
    num_visits = request.session.get("num_visits", 0)
    request.session["num_visits"] = num_visits + 1
    context = {
        "num_books": num_books,
        "num_authors": num_authors,
        "num_literary_formats": num_literary_formats,
        "num_visits": num_visits + 1,
    }
    return render(request, "catalog/index.html", context=context)


class BookListView(generic.ListView):
    model = Book
    queryset = Book.objects.select_related("format")
    paginate_by = 10

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        title = self.request.GET.get("title", "")
        context["title"] = title
        context["search_form"] = BookSearchForm(initial={"title": title})
        return context

    def get_queryset(self):
        # title = self.request.GET.get("title")
        # if title:
        #     return self.queryset.filter(title__icontains=title)
        # return self.queryset
        form = BookSearchForm(self.request.GET)
        if form.is_valid():
            return self.queryset.filter(title__icontains=form.cleaned_data["title"])
        return self.queryset


class BookDetailView(generic.DetailView):
    model = Book


class BookCreateView(LoginRequiredMixin, generic.CreateView):
    model = Book
    success_url = reverse_lazy("catalog:book_list")
    form_class = BookForm
    template_name = "catalog/book_form.html"


class BookUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = Book
    success_url = reverse_lazy("catalog:book_list")
    form_class = BookForm
    # fields = "__all__"
    template_name = "catalog/book_form.html"


class BookDeleteView(LoginRequiredMixin, generic.UpdateView):
    model = Book
    success_url = reverse_lazy("catalog:book_list")
    form_class = BookForm
    # fields = "__all__"
    template_name = "catalog/book_confirm_delete.html"


class AuthorListView(generic.ListView):
    model = Author
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context["search_form"] = AuthorSearchForm()
        return context


class AuthorDetailView(generic.DetailView):
    model = Author


class AuthorCreateView(LoginRequiredMixin, generic.CreateView):
    model = Author
    success_url = reverse_lazy("catalog:author_list")
    template_name = "catalog/author_form.html"


class AuthorUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = Author
    fields = "__all__"
    success_url = reverse_lazy("catalog:author_list")
    template_name = "catalog/author_form.html"


class AuthorDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Author
    fields = "__all__"
    success_url = reverse_lazy("catalog:author_list")
    template_name = "catalog/author_confirm_delete.html"


class LiteraryFormatListView(generic.ListView):
    model = LiteraryFormat
    template_name = "catalog/literary_format_list.html"
    context_object_name = "literary_format_list"
    paginate_by = 10


class LiteraryFormatCreateView(LoginRequiredMixin, generic.CreateView):
    model = LiteraryFormat
    form_class = LiteraryFormatForm
    success_url = reverse_lazy("catalog:literary_format_list")
    template_name = "catalog/literary_format_form.html"


class LiteraryFormatUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = LiteraryFormat
    fields = "__all__"
    success_url = reverse_lazy("catalog:literary_format_list")
    template_name = "catalog/literary_format_form.html"


class LiteraryFormatDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = LiteraryFormat
    fields = "__all__"
    success_url = reverse_lazy("catalog:literary_format_list")
    template_name = "catalog/literary_format_confirm_delete.html"


# @api_view(["GET", "POST"])
# def literary_format_list(request):
#     if request.method == "GET":
#         literary_formats = LiteraryFormat.objects.all()
#         serializer = LiteraryFormatSerializer(literary_formats, many=True)
#         return Response(serializer.data, status=status.HTTP_200_OK)
#     else:
#         serializer = LiteraryFormatSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#   func based ape view
# @api_view(["GET", "PUT", "DELETE"])
# def literary_format_detail(request, pk):
#     literary_format = get_object_or_404(LiteraryFormat, pk=pk)
#     if request.method == "GET":
#         serializer = LiteraryFormatSerializer(literary_format)
#         return Response(serializer.data, status=status.HTTP_200_OK)
#     elif request.method == "PUT":
#         serializer = LiteraryFormatSerializer(literary_format, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_200_OK)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#     else:
#         literary_format.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)


#   class based api view
# class LiteraryFormatList(generics.ListCreateAPIView):
#     queryset = LiteraryFormat.objects.all()
#     serializer_class = LiteraryFormatSerializer
#
#
# class LiteraryFormatDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = LiteraryFormat.objects.all()
#     serializer_class = LiteraryFormatSerializer


class LiteraryFormatViewSet(viewsets.ModelViewSet):
    queryset = LiteraryFormat.objects.all()
    serializer_class = LiteraryFormatSerializer


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
